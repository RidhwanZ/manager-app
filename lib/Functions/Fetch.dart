import 'package:manager_app/object/Posts.dart';
import 'package:manager_app/state/postState.dart';

import 'Api.dart';

class Fetch{

  static Future<bool> FetchData() async{
    print("FETCH POSTS");
    var postsListData = await Api.getPosts();
    List<PostsObject> postList = [];
    List<dynamic> postsData = postsListData ?? [];
    postsData?.forEach((json) {
      postList.add(PostsObject.fromJson(json));
    });
    PostState.instance.setPostList(postList);

    return true;
  }

}
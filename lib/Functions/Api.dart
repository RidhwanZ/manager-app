import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'dart:io';

class Api {

  static final String apiPrefix = "https://jsonplaceholder.typicode.com/";

  static Future<dynamic> getPosts() async {
    final response = await http.get(apiPrefix + "posts");
    return jsonDecode(response.body);
  }

  static Future<dynamic> getComments(int id) async {
    final response = await http.get(apiPrefix + "comments?postId=$id");
    return jsonDecode(response.body);
  }


}
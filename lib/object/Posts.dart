class PostsObject{
  int userId;
  int id;
  String title;
  String body;

  PostsObject({
    this.userId,
    this.id,
    this.title,
    this.body
  });

  factory PostsObject.fromJson(Map<String, dynamic> json){
    return PostsObject(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body']
    );
  }
}
import 'package:flutter/material.dart';
import 'package:manager_app/Functions/Fetch.dart';
import 'package:manager_app/routes/routes.dart';

import 'Functions/Api.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    fetchPosts();
  }

  void fetchPosts() {
    Fetch.FetchData().then((val){
      if(val){
        AppRouter().navPosts(context);
      }else{
        AppRouter().navRoot(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Container(
            color: Colors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 100.0,
                  width: 100.0,
                  child: Image.asset(
                    "assets/logo.jpg",
                    fit: BoxFit.contain
                  ),
                ),
              ],
            )));
  }
}

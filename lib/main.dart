import 'package:flutter/material.dart';
import 'package:manager_app/routes/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyApp() {
    final appRouter = AppRouter();
    appRouter.configureRoutes();
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final app = MaterialApp(
      title: 'Manager App',
      onGenerateRoute: AppRouter().router().generator,
    );
    return app;
  }
}


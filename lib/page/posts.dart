import 'package:flutter/material.dart';
import 'package:manager_app/object/Posts.dart';
import 'package:manager_app/page/postDetails.dart';
import 'package:manager_app/state/postState.dart';

class Posts extends StatefulWidget {
  @override
  _PostsState createState() => _PostsState();
}

class _PostsState extends State<Posts> {
  final PostState _postState = PostState.instance;
  List<PostsObject> _posts = [];

  @override
  void initState() {
    _posts = _postState.get(PostState.FULL_POSTS);

//    print(_posts.length);
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  void postDetails(int index) {
    Navigator.of(context)
        .push(MaterialPageRoute<Null>(builder: (BuildContext context) {
      return PostDetails(postId: _posts[index].id);
    }));
  }

  Widget _postList() {
    return ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return Divider(
            thickness: 1.0,
            color: Colors.grey,
          );
        },
        itemCount: _posts?.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            onTap: (){
              postDetails(index);
            },
            title: Text(_posts[index].title),
            trailing: Icon(Icons.arrow_forward_ios, color: Colors.grey, size: 15.0,),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Posts'),
          centerTitle: true,
        ),
        body: _postList());
  }
}

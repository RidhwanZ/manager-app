import 'package:flutter/material.dart';
import 'package:manager_app/Functions/Api.dart';
import 'package:manager_app/object/Comments.dart';
import 'package:manager_app/object/Posts.dart';
import 'package:manager_app/state/postState.dart';

class PostDetails extends StatefulWidget {
  final int postId;

  PostDetails({this.postId});

  @override
  _PostDetailsState createState() => _PostDetailsState();
}

class _PostDetailsState extends State<PostDetails> {
  final PostState _postState = PostState.instance;
  List<PostsObject> _posts = [];
  List<PostsObject> _post = [];
  List<Comments> _comments = [];
  List<Comments> _originalComments = [];
  int _postId;
  TextEditingController searchComments = TextEditingController();

  @override
  void initState() {
    _postId = widget.postId;
    _posts = _postState.get(PostState.FULL_POSTS);
    _post = _posts.where((p) => p.id == _postId).toList();
    _getComments();
    searchComments.addListener(_filterComments);

    // TODO: implement initState
    super.initState();
  }

  void _getComments() async {
    var postCommentData = await Api.getComments(_postId);

    List<Comments> postCommentList = [];
    List<dynamic> postComment = postCommentData ?? [];
    postComment?.forEach((json) {
      postCommentList.add(Comments.fromJson(json));
    });

    if (mounted) {
      setState(() {
        _comments = postCommentList;
        _originalComments = postCommentList;
      });
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Widget _postTitleBody() {
    return Wrap(
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Title : ', style: TextStyle(fontWeight: FontWeight.bold)),
            SizedBox(width: 5.0),
            Container(
              width: MediaQuery.of(context).size.width - 60,
              child: Text(_post[0].title)
            )
          ],
        ),
        SizedBox(height: 30.0),
        Row(
          children: <Widget>[
            Text('Body : ', style: TextStyle(fontWeight: FontWeight.bold)),
            SizedBox(width: 5.0),
            Container(
                width: MediaQuery.of(context).size.width - 60,
                child: Text(_post[0].body)
            )
          ],
        )
      ],
    );
  }

  Widget _postCommentList() {
    return ListView.separated(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        separatorBuilder: (BuildContext context, int index) {
          return Divider(
            height: 1.0,
            color: Colors.grey,
          );
        },
        itemCount: _comments?.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(_comments[index].name,
                style: TextStyle(fontWeight: FontWeight.bold)),
            subtitle: Text(_comments[index].body),
          );
        });
  }

  void _filterComments() {
    String query = searchComments.text;
    RegExp exp = RegExp("$query", caseSensitive: false);
    List<Comments> temp = _originalComments
        .where((f) => (exp.hasMatch(f.name) ||
        exp.hasMatch(f.email) ||
        exp.hasMatch(f.body)))
        .toList();
    if (temp.length > 0) {
      setState(() {
        _comments = temp;
      });
    } else {
      setState(() {
        _comments = _originalComments;
      });
    }
  }

  Widget _postDetails() {
    return Container(
      padding: EdgeInsets.all(5.0),
      height: MediaQuery.of(context).size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _postTitleBody(),
          SizedBox(
            height: 30.0,
          ),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 40.0,
                child: TextField(
                  controller: searchComments,
                  decoration: InputDecoration(
                      hintText: "Search cooments by Name, Email or Body",
                      isDense: true,
                      suffixIcon: Icon(Icons.search)),
                ),
              )),
          SizedBox(
            height: 20.0,
          ),
          Text('Comments : ', style: TextStyle(fontWeight: FontWeight.bold)),
          _postCommentList()
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Post Details"),
        centerTitle: true,
      ),
      body: _postDetails(),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:manager_app/routes/route_handlers.dart';

class AppRouter {
  static final AppRouter _instance = AppRouter._internal();
  final Router _router = Router();
  static String root = "/";
  static String posts = "/posts";
  static String comment = "/comments";

  factory AppRouter() {
    return _instance;
  }

  AppRouter._internal();

  Router router() {
    return _router;
  }

  void configureRoutes() {
    _router.notFoundHandler = new Handler(
        handlerFunc: (BuildContext context, Map<String, List<String>> params) {
          print("ROUTE WAS NOT FOUND !!!");
          return null;
        });

    _router.define(root, handler: rootHandler);
    _router.define(posts, handler: postHandler);
//    _router.define(comment, handler: commentHandler);
  }

  void navPosts(context) {
    _router.navigateTo(context, posts,
        transition: TransitionType.inFromLeft, replace: true);
  }

  void navRoot(context) {
    _router.navigateTo(
      context,
      root,
      transition: TransitionType.fadeIn,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:fluro/fluro.dart';
import 'package:manager_app/page/posts.dart';
import 'package:manager_app/splashscreen.dart';

var rootHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return SplashScreen();
});

var postHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, List<String>> params) {
  return Posts();
});

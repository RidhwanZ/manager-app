import 'dart:async';

import 'package:manager_app/object/Posts.dart';

class PostState{
  static const int FULL_POSTS = 1;

  StreamController _postStreamController = StreamController<List<PostsObject>>.broadcast();
  Stream get onPostStateChanged => _postStreamController.stream;

  static PostState instance = new PostState._();
  PostState._();

  setPostList(List<PostsObject> post) {
    PostState._allPosts = post;
    _postStreamController.add(post);
  }

  get(int type) {
    switch (type) {
      case FULL_POSTS:
        return _allPosts;
      default:
        break;
    }
  }

  static List<PostsObject> _allPosts;
}